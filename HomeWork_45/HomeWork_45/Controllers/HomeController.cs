﻿using HomeWork_45.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace HomeWork_45.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ApplicationDbContext _context;

        public HomeController(ILogger<HomeController> logger, ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public IActionResult Details()
        {
            var users = _context.Users.ToList();

            if (users.Count < 1)
            {
                // return RedirectToAction("Index");
                return Ok("Нету данных");
            }

            return View(users);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(User user)
        {
            if (user == null)
            {
                return RedirectToAction("Index");
            }
            user.DateOfCreate = DateTime.Now;

            _context.Users.Add(user);
            _context.SaveChanges();
            return RedirectToAction("Details");
        }

        public IActionResult Edit(int id)
        {
            var user = _context.Users.FirstOrDefault(x => x.ID == id);
            if (user == null)
            {
                return RedirectToAction("Details");
            }
            return View(user);
        }

        [HttpPost]
        public IActionResult Edit(User user)
        {
            if (user == null)
                return RedirectToAction("Details");

            _context.Users.Update(user);
            _context.SaveChanges();
            return RedirectToAction("Details");
        }

        public IActionResult Delete(int id)
        {
            var user = _context.Users.FirstOrDefault(x => x.ID == id);
            if (user == null)
            {
                return RedirectToAction("Details");
            }

            _context.Users.Remove(user);
            _context.SaveChanges();








            return RedirectToAction("Details");
        }


    }
}
